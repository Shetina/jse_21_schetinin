package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "logout";

    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}