package ru.t1.schetinin.tm.api.service;

import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}